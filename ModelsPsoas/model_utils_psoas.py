import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.optim import lr_scheduler


class PadTensor(nn.Module):
    def __init__(self, padding, dim=None, input_shape=None, operation_type=None,
                 kernel_size=None, stride=None, dilation=None, output_shape=None):
        super(PadTensor, self).__init__()

        pad_amount = 0

        pad_x = 0
        pad_y = 0
        pad_z = 0

        x_out = 0
        y_out = 0
        z_out = 0



        # TODO Make padding work for arbitrary dimension without so many if statements

        if dim is None:
            if len(input_shape) == 2:
                dim = 2
            elif len(input_shape) == 3:
                dim = 3
            else:
                raise ValueError("Shape must be 2d or 3d")


        if isinstance(padding, int):
            if dim == 2:
                pad_amount = [padding, padding, padding, padding]
            if dim == 3:
                pad_amount = [padding, padding, padding, padding, padding, padding]


        elif isinstance(padding, tuple) or isinstance(padding, list):
            if dim == 2:
                if len(padding) == 2:
                    pad_amount = [padding[0], padding[0], padding[1], padding[1]]

                elif len(padding) == 4:
                    pad_amount = [padding[0], padding[1], padding[2], padding[3]]

            if dim == 3:
                if len(padding) == 3:
                    pad_amount = [padding[0], padding[0], padding[1], padding[1], padding[2], padding[2]]

                elif len(padding) == 6:
                    pad_amount = [padding[0], padding[1], padding[2], padding[3], padding[4], padding[5]]



        elif padding == 'same':
            assert input_shape is not None, 'There must be the output shape you want to pad to to determine stride'

            # x_size, y_size, z_size = feature_map.shape[2], feature_map.shape[3], feature_map.shape[4]
            #
            if dim == 2:
                x_in, y_in = input_shape[0], input_shape[1]

                if output_shape is not None:
                    x_out, y_out = output_shape[0], output_shape[1]
                else:
                    x_out, y_out = input_shape[0], input_shape[1]

            elif dim == 3:
                x_in, y_in, z_in = input_shape[0], input_shape[1], input_shape[2]

                if output_shape is not None:
                    x_out, y_out, z_out = output_shape[0], output_shape[1], output_shape[2]
                else:
                    x_out, y_out, z_out = input_shape[0], input_shape[1], input_shape[2]

            if operation_type == 'down_conv':
                pad_x = (stride * (x_out - 1) - x_in + dilation * (kernel_size - 1) + 1) / 2
                pad_x = 1 if pad_x == 0.5 else int(pad_x)

                pad_y = (stride * (y_out - 1) - y_in + dilation * (kernel_size - 1) + 1) / 2
                pad_y = 1 if pad_y == 0.5 else int(pad_y)

                pad_amount = [int(pad_x), int(pad_x), int(pad_y), int(pad_y)]

                if dim == 3:
                    pad_z = (stride * (z_out - 1) - z_in + dilation * (kernel_size - 1) + 1) / 2
                    pad_z = 1 if pad_z == 0.5 else int(pad_z)

                    pad_amount.append(int(pad_z))
                    pad_amount.append(int(pad_z))

            elif operation_type == 'up_conv':

                pad_x = ((x_out - 1) * stride - x_in + dilation * (kernel_size - 1) + 1) / 2
                pad_y = ((x_out - 1) * stride - y_in + dilation * (kernel_size - 1) + 1) / 2

                pad_amount = [int(pad_x), int(pad_x), int(pad_y), int(pad_y)]

                if dim == 2:
                    pad_z = ((x_out - 1) * stride - z_in + dilation * (kernel_size - 1) + 1) / 2
                    pad_amount.append(int(pad_z))
                    pad_amount.append(int(pad_z))

            elif operation_type == 'max_pooling':

                pad_x = (stride * (x_out - 1) - x_in + dilation * (kernel_size - 1) + 1) / 2
                pad_x = 1 if pad_x == 0.5 else int(pad_x)

                pad_y = (stride * (y_out - 1) - y_in + dilation * (kernel_size - 1) + 1) / 2
                pad_y = 1 if pad_y == 0.5 else int(pad_y)

                pad_amount = [int(pad_x), int(pad_x), int(pad_y), int(pad_y)]

                if dim == 3:
                    pad_z = (stride * (z_out - 1) - z_in + dilation * (kernel_size - 1) + 1) / 2
                    pad_z = 1 if pad_z == 0.5 else int(pad_z)
                    pad_amount.append(int(pad_z))
                    pad_amount.append(int(pad_z))

        if dim == 2:
            self.pad = nn.ConstantPad2d(pad_amount, value=0)


        if dim == 3:
            self.pad = nn.ConstantPad3d(pad_amount, value=0)

    def forward(self, x):
        x = self.pad(x)
        return x

def PadTensorFunctional(padding, output_shape, conv_type, kernel_size=None, stride=None, dilation=None):
    pad_amount = 0

    pad_x = 0
    pad_y = 0
    pad_z = 0

    if isinstance(padding, int):
        pad_amount = [padding, padding, padding]


    elif isinstance(padding, tuple) or isinstance(padding, list):
        if len(padding) == 3:
            # pad_amount = [padding[0], padding[0], padding[1], padding[1], padding[2], padding[2]]
            pad_amount = [padding[0], padding[1], padding[2]]


        elif len(padding) == 6:
            pad_amount = [padding[0], padding[1], padding[2]]


    elif padding == 'same':
        assert output_shape is not None, 'There must be the output shape you want to pad to to determine stride'

        # x_size, y_size, z_size = feature_map.shape[2], feature_map.shape[3], feature_map.shape[4]
        #
        x_out, y_out, z_out = output_shape[0], output_shape[1], output_shape[2]

        if conv_type == 'down_conv':
            pad_x = (stride * (x_out - 1) - x_out + dilation * (kernel_size - 1) + 1) / 2
            pad_y = (stride * (y_out - 1) - y_out + dilation * (kernel_size - 1) + 1) / 2
            pad_z = (stride * (z_out - 1) - z_out + dilation * (kernel_size - 1) + 1) / 2

        elif conv_type == 'up_conv':

            pad_x = ((x_out - 1) * stride - x_out + dilation * (kernel_size - 1) + 1) / 2
            pad_y = ((x_out - 1) * stride - y_out + dilation * (kernel_size - 1) + 1) / 2
            pad_z = ((x_out - 1) * stride - z_out + dilation * (kernel_size - 1) + 1) / 2


        pad_amount = [int(pad_x), int(pad_y), int(pad_z)]

    return pad_amount


def model_learning_scheduler(optimizer, params):
    """
    Return a learning rate scheduler based on the parameters chosen.

    Parameters
    ----------
    optimizer
    params

    Returns
    -------

    """

    if params.lr_scheduler == 'linear':
        def lambda_rule(epoch):
            lr_linear = 1.0 - max(0, epoch + params.epoch_count - params.num_iter) / float(params.num_iter_decay + 1)
            return lr_linear
        scheduler = lr_scheduler.LambdaLR(optimizer, lr_lambda=lambda_rule)
    elif params.lr_scheduler == 'step':
        scheduler =lr_scheduler.StepLR(optimizer, step_size=params.lr_decay_iters, gamma=0.1)
    elif params.lr_scheduler == 'plateau':
        scheduler = lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.05, threshold=1e-4, patience=10)
    else:
        scheduler = None

    return scheduler
