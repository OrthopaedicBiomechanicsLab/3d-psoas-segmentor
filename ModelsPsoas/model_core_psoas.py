

# from model_utils import *
from ModelsPsoas.model_utils_psoas import *
import torch.nn as nn

import numpy as np


class Conv2D(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size=3,
                 stride=1,
                 dilation=1,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.1,
                 norm_track_running_states=False,
                 activation=None,
                 padding=None,
                 input_shape=None,
                 output_shape=None,
                 init_weights_type='kaiming'):
        """
        This is a simple conv3D layer. No normalization or activation.
        :param in_channels: tuple; Number of channels for input feature map or image.
        :param out_channels: tuple; Number of channels for output feature map or image.
        :param kernel_size: int or tuple; Size of the convolution kernel.
                            If int, all dimensions of the kernel will be the same.
        """
        super(Conv2D, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.input_shape = input_shape

        if output_shape is None:
            if padding == 'same':
                output_shape = input_shape / stride

        # self.in_channels = in_channels
        # self.out_channels = out_channels
        # self.kernel_size = kernel_size
        # self.stride = stride
        # self.dilation = dilation
        # self.batch_normalization = batch_normalization
        # self.instance_normalization = instance_normalization
        # self.normalization_momentum = normalization_momentum
        # self.norm_track_running_states = norm_track_running_states
        # self.activation = activation
        # self.padding = padding
        # self.input_shape = input_shape,
        # self.output_shape = output_shape
        # self.init_weights_type = init_weights_type

        # pad_amount = PadTensorFunctional(padding=padding, output_shape=input_shape,
        #                                  conv_type='down_conv', kernel_size=kernel_size,
        #                                  stride=stride, dilation=dilation)

        conv = nn.Conv2d(in_channels=in_channels, out_channels=out_channels,
                         kernel_size=kernel_size, stride=stride, dilation=dilation)

        self.conv_hold = nn.Conv2d(in_channels=in_channels, out_channels=out_channels,
                                   kernel_size=kernel_size, stride=stride, dilation=dilation)

        if padding is not None:
        # if (input_shape is not None) and (padding is not None):

            pad_layer = PadTensor(padding, dim=2, operation_type='down_conv',
                                  kernel_size=kernel_size, stride=stride, dilation=dilation,
                                  input_shape=input_shape,
                                  output_shape=output_shape)

            # Should be pad then conv. Differences are only for compatibility with older models.
            # conv_block = [conv, pad_layer]
            conv_block = [pad_layer, conv]
        else:
            conv_block = [conv]

        # conv_block = [conv]

        if (batch_normalization is True) and (instance_normalization is True):
            Warning('Both batch normalization and instance normalization are set to True. As both cannot be used, '
                    'will result in default configuration and use batch normalization')
            instance_normalization = False

        if batch_normalization:
            bn = batch_normalization_layer(out_channels,
                                           dim=2,
                                           momentum=normalization_momentum,
                                           track_running_stats=norm_track_running_states)
            conv_block = conv_block + [bn]

        if instance_normalization:
            instnorm = instance_normalization_layer(out_channels,
                                                    dim=2,
                                                    momentum=normalization_momentum,
                                                    track_running_stats=norm_track_running_states)
            conv_block = conv_block + [instnorm]

        if activation:
            act = ActivationLayer(activation_type=activation)
            conv_block = conv_block + [act]

        self.conv_block = nn.Sequential(*conv_block)


    def forward(self, x):
        x = self.conv_block(x)
        return x


class Conv3D(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size=3,
                 stride=1,
                 dilation=1,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.1,
                 norm_track_running_states=False,
                 activation=None,
                 padding=None,
                 input_shape=None,
                 output_shape=None,
                 init_weights_type='kaiming'):
        """
        This is a simple conv3D layer. No normalization or activation.
        :param in_channels: tuple; Number of channels for input feature map or image.
        :param out_channels: tuple; Number of channels for output feature map or image.
        :param kernel_size: int or tuple; Size of the convolution kernel.
                            If int, all dimensions of the kernel will be the same.
        """
        super(Conv3D, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels

        self.input_shape = input_shape

        if output_shape is None:
            if padding == 'same':
                output_shape = input_shape / stride
        # pad_amount = PadTensorFunctional(padding=padding, output_shape=input_shape,
        #                                  conv_type='down_conv', kernel_size=kernel_size,
        #                                  stride=stride, dilation=dilation)


        if isinstance(padding, int):
            conv = nn.Conv3d(in_channels=in_channels, out_channels=out_channels,
                             padding=padding, kernel_size=kernel_size, stride=stride, dilation=dilation)
            padding = None

        else:
            conv = nn.Conv3d(in_channels=in_channels, out_channels=out_channels,
                             kernel_size=kernel_size, stride=stride, dilation=dilation)

        if padding is not None:
        # if (input_shape is not None) and (padding is not None):
            pad_layer = PadTensor(padding, dim=3, input_shape=input_shape, operation_type='down_conv',
                                  kernel_size=kernel_size, stride=stride, dilation=dilation,
                                  output_shape=output_shape)

            # Should be pad then conv. Differences are only for compatibility with older models.
            conv_block = [pad_layer, conv]
            # conv_block = [conv, pad_layer]

        else:
            conv_block = [conv]

        if (batch_normalization is True) and (instance_normalization is True):
            Warning('Both batch normalization and instance normalization are set to True. As both cannot be used, '
                    'will result in default configuration and use batch normalization')
            instance_normalization = False

        if batch_normalization:
            bn = batch_normalization_layer(out_channels,
                                           dim=3,
                                           momentum=normalization_momentum,
                                           track_running_stats=norm_track_running_states)
            conv_block = conv_block + [bn]

        if instance_normalization:
            instnorm = instance_normalization_layer(out_channels,
                                                    dim=3,
                                                    momentum=normalization_momentum,
                                                    track_running_stats=norm_track_running_states)
            conv_block = conv_block + [instnorm]

        if activation:
            act = ActivationLayer(activation_type=activation)
            conv_block = conv_block + [act]

        self.conv_block = nn.Sequential(*conv_block)




    def forward(self, x):
        x = self.conv_block(x)
        return x


class UpConv3D(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size=2,
                 stride=2,
                 dilation=1,
                 batch_normalization=False,
                 instance_normalization=False,
                 normalization_momentum=0.99,
                 norm_track_running_states=False,
                 activation=None,
                 padding=None,
                 input_shape=None,
                 output_shape=None,
                 init_weights_type='kaiming'):
    # def __init__(self, input_shape, in_channels, out_channels, kernel_size=2, stride=2, dilation=1, padding=0):
        """
        This is a simple up-conv3D layer. No normalization or activation.
        :param in_channels: tuple; Number of channels for input feature map or image.
        :param out_channels: tuple; Number of channels for output feature map or image.
        :param kernel_size: int or tuple; Size of the convolutional kernel.
                            If int, all dimensions of the kernel will be the same.
        """
        super(UpConv3D, self).__init__()

        up_conv = nn.ConvTranspose3d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size,
                                     stride=stride, dilation=dilation)

        if (input_shape is not None) and (padding is not None):
            pad_layer = PadTensor(padding, input_shape=input_shape, operation_type='up_conv',
                                  kernel_size=kernel_size, stride=stride, dilation=dilation,
                                  output_shape=output_shape)

            # Should be pad then conv. Differences are only for compatibility with older models.
            up_conv = [pad_layer, up_conv]
            # up_conv = [up_conv, pad_layer]
        else:
            up_conv = [up_conv]

        if (batch_normalization is True) and (instance_normalization is True):
            Warning('Both batch normalization and instance normalization are set to True. As both cannot be used, '
                    'will result in default configuration and use batch normalization')
            instance_normalization = False

        if batch_normalization:
            bn = batch_normalization_layer(out_channels,
                                           dim=3,
                                           momentum=normalization_momentum,
                                           track_running_stats=norm_track_running_states)
            up_conv = up_conv + [bn]

        if instance_normalization:
            instnorm = instance_normalization_layer(out_channels,
                                                    dim=3,
                                                    momentum=normalization_momentum,
                                                    track_running_stats=norm_track_running_states)
            up_conv = up_conv + [instnorm]

        if activation:
            act = ActivationLayer(activation_type=activation)
            up_conv = up_conv + [act]

        self.up_conv = nn.Sequential(*up_conv)


    def forward(self, x):
        x = self.up_conv(x)
        return x


class ResConvBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size=3,
                 stride=1,
                 dilation=1,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.1,
                 norm_track_running_states=False,
                 activation=None,
                 padding=None,
                 input_shape=None,
                 output_shape=None,
                 init_weights_type='kaiming',
                 num_conv=2,
                 final_act='relu',
                 ndims=None):
        """

        """
        super(ResConvBlock, self).__init__()

        if ndims is not None:
            conv_fn = getattr('Conv%dD' % ndims)
        elif input_shape is not None:
            ndims = len(input_shape)
            conv_fn = globals()['Conv%dD' % ndims]
        else:
            conv_fn = globals()['Conv%dD' % 3]

        conv_list = []

        self.in_channels = in_channels
        self.out_channels = out_channels

        orig_input_shape = input_shape
        orig_in_channels = in_channels

        input_shape = input_shape
        output_shape = output_shape

        if isinstance(out_channels, int):
            out_channels_list = [out_channels] * num_conv
        else:
            out_channels_list = out_channels

        if isinstance(kernel_size, int):
            kernel_size_list = [kernel_size] * num_conv
        else:
            kernel_size_list = kernel_size

        if isinstance(stride, int):
            stride_list = [stride] * num_conv
        else:
            stride_list = stride

        if isinstance(dilation, int):
            dilation_list = [dilation] * num_conv
        else:
            dilation_list = dilation

        for idx, out_channels, kernel_size, stride, dilation in zip(range(num_conv), out_channels_list, kernel_size_list, stride_list, dilation_list):
            conv = conv_fn(in_channels=in_channels,
                           out_channels=out_channels,
                           kernel_size=kernel_size,
                           stride=stride,
                           dilation=dilation,
                           batch_normalization=batch_normalization,
                           instance_normalization=instance_normalization,
                           normalization_momentum=normalization_momentum,
                           norm_track_running_states=norm_track_running_states,
                           activation=activation,
                           padding=padding,
                           input_shape=input_shape,
                           output_shape=output_shape,
                           init_weights_type=init_weights_type)

            conv_list.append(conv)

            if output_shape is not None:
                input_shape = output_shape
            else:
                if isinstance(padding, int):
                    input_shape = (input_shape + 2*padding - dilation * (kernel_size - 1) - 1) / stride + 1

                elif isinstance(padding, tuple) or isinstance(padding, list):
                    padding_np = np.array(padding)
                    if len(padding) == 3:
                        input_shape = (input_shape + 2 * padding_np - dilation * (kernel_size - 1) - 1) / stride + 1
                    elif len(padding) == 6:
                        input_shape_x = (input_shape[0] + (padding_np[0] + padding_np[1]) - dilation * (kernel_size - 1) - 1) / stride + 1
                        input_shape_y = (input_shape[1] + (padding_np[2] + padding_np[3]) - dilation * (kernel_size - 1) - 1) / stride + 1
                        input_shape_z = (input_shape[2] + (padding_np[4] + padding_np[5]) - dilation * (kernel_size - 1) - 1) / stride + 1

                        input_shape = np.array([input_shape_x, input_shape_y, input_shape_z])

                elif padding == 'same':
                    input_shape = input_shape / stride

            in_channels = out_channels


        if self.in_channels != self.out_channels:
            self.short_cut = Conv3D(in_channels=orig_in_channels,
                                    out_channels=out_channels_list[-1],
                                    kernel_size=1,
                                    stride=stride_list[-1],
                                    dilation=dilation_list[-1],
                                    batch_normalization=batch_normalization,
                                    instance_normalization=instance_normalization,
                                    normalization_momentum=normalization_momentum,
                                    norm_track_running_states=norm_track_running_states,
                                    activation=None,
                                    padding=padding,
                                    input_shape=orig_input_shape,
                                    output_shape=None,
                                    init_weights_type=init_weights_type)
        else:
            self.short_cut = None

        self.conv = nn.Sequential(*conv_list)

        self.final_act = ActivationLayer(final_act)

    def forward(self, x):
        x_out = self.conv(x)

        if self.short_cut is not None:
            x_short = self.short_cut(x)
        else:
            x_short = x

        x = torch.add(x_out, x_short)

        x = self.final_act(x)

        return x


class batch_normalization_layer(nn.Module):
    def __init__(self, num_features, dim, momentum=0.1, track_running_stats=False, init_weights_type='kaiming'):
        """
        Batch normalization layer.
        :param num_features: int or tuple; Number of channels for the input for normalization.
                            If tuple, the assumed shape will be (N, C, D, H, W) and C will be extracted.
        :param momentum: int; Momentum used for batch normalization calculation.
                        Default is 0.99 as it is the default from Keras
        """
        super(batch_normalization_layer, self).__init__()

        # Check to ensure that num_features is not a tuple. If it is a tuple, extract C from it.
        # Tuple would be of shape (N, C, D, H, W).

        if isinstance(num_features, int):
            num_features = num_features
        elif len(num_features) == 5:
            num_features = num_features[1]

        if dim == 3:
            self.bn = nn.BatchNorm3d(num_features=num_features,
                                     momentum=momentum, track_running_stats=track_running_stats)
        elif dim == 2:
            self.bn = nn.BatchNorm2d(num_features=num_features,
                                     momentum=momentum, track_running_stats=track_running_stats)
        elif dim == 1:
            self.bn = nn.BatchNorm1d(num_features=num_features,
                                     momentum=momentum, track_running_stats=track_running_stats)
        else:
            raise ValueError('Dimension required for batch normalization.')



    def forward(self, x):
        x = self.bn(x)
        return x


class instance_normalization_layer(nn.Module):
    def __init__(self, num_features, dim, momentum=0.99, track_running_stats=False):
        """
        Instance normalization layer.
        :param num_features: int or tuple; Number of channels for the input for normalization.
                            If tuple, the assumed shape will be (N, C, D, H, W) and C will be extracted.
        :param momentum: int; Momentum used for batch normalization calculation.
                        Default is 0.99 as it is the default from Keras
        """
        super(instance_normalization_layer, self).__init__()

        # Check to ensure that num_features is not a tuple. If it is a tuple, extract C from it.
        # Tuple would be of shape (N, C, D, H, W).

        if isinstance(num_features, int):
            num_features = num_features
        elif len(num_features) == 5:
            num_features = num_features[1]


        if dim == 3:
            self.instnorm = nn.InstanceNorm3d(num_features=num_features,
                                              momentum=momentum, track_running_stats=track_running_stats)
        elif dim == 2:
            self.instnorm = nn.InstanceNorm2d(num_features=num_features,
                                              momentum=momentum, track_running_stats=track_running_stats)
        elif dim == 1:
            self.instnorm = nn.InstanceNorm1d(num_features=num_features,
                                              momentum=momentum, track_running_stats=track_running_stats)
        else:
            raise ValueError('Dimension required for batch normalization.')


    def forward(self, x):
        x = self.instnorm(x)
        return x


class ActivationLayer(nn.Module):
    def __init__(self, activation_type, **kwargs):
        """
        Layer used for activations
        :param activation_type: string; Name of activation for to be selected.
                                        Potential options are "relu", "sigmoid", and "softmax", with more to be added.
                                        Selection is not case sensitive.
        :param kwargs: dict; Additional parameters, if any, for selected activation.
        """
        super(ActivationLayer, self).__init__()

        assert isinstance(activation_type, str), 'Activation must be of type string.'

        activation_type = activation_type.lower()
        #
        # self.hold = None
        # if activation_type == 'softmax':
        #     self.hold = 1

        # For linear activation, the output is the identity as the previous layer performs the linear function.
        # Linear activation is the same as no activation.
        activation_dict = {'relu': nn.ReLU(inplace=True),
                           'sigmoid': nn.Sigmoid(),
                           'softmax': nn.Softmax(**kwargs),
                           'logsoftmax': nn.LogSoftmax(**kwargs),
                           'none': nn.Identity()}


        self.activation = activation_dict[activation_type]
    def forward(self, x):
        # if self.hold == 1:
        #     a=1
        x = self.activation(x)
        return x


