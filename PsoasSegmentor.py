from __future__ import print_function
import os, vtk, qt, ctk, slicer
import logging
from ModelsPsoas.UNetPsoas import UNetPsoas
import torch
import SimpleITK as sitk
import sitkUtils
import numpy as np
import scipy

from slicer.ScriptedLoadableModule import *

class PsoasSegmentor(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class, available at:
       https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent):
        self.parent = parent
        self.moduleName = self.__class__.__name__

        self.parent.title = "3D Psoas Segmentor"
        self.parent.categories = ["Sarcopenia"]  # categories (folders where the module shows up in the module selector)
        self.parent.dependencies = []  # TODO: add here list of module names that this module requires
        self.parent.contributors = ["Kelly Fullerton (Sunnybrook Research Institute)"]
        self.parent.helpText = """ This module segments the psoas muscle in inputted abdominal scans."""
        self.parent.helpText += self.getDefaultModuleDocumentationLink()  # TODO: verify that the default URL is correct or change it to the actual documentation
        self.parent.acknowledgementText = """ This file was originally developed by Kelly Fullerton, Sunnybrook Research Institute,
            as part of a research conducted by Cari Whyne. Principle investigator is Urban Emmenegger.  U-Net CNN was developed by Geoff Klein.  
            Special thanks to Michael Hardisty, Frank Lyons and Joel Finkelstein for their clinical and technical support in this project."""

        # Set module icon from Resources/Icons/<ModuleName>.png
        moduleDir = os.path.dirname(self.parent.path)
        iconPath = os.path.join(moduleDir, 'Resources/Icons', self.moduleName + '.png')
        if os.path.isfile(iconPath):
            parent.icon = qt.QIcon(iconPath)

        # Add this test to the SelfTest module's list for discovery when the module
        # is created.  Since this module may be discovered before SelfTests itself,
        # create the list if it doesn't already exist.
        try:
            slicer.selfTests
        except AttributeError:
            slicer.selfTests = {}
        slicer.selfTests[self.moduleName] = self.runTest



#
# Psoas Segmentation Widget
#

class PsoasSegmentorWidget(ScriptedLoadableModuleWidget):
    """Uses ScriptedLoadableModuleWidget base class, available at:
        https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setup(self):
        ScriptedLoadableModuleWidget.setup(self)

        # Instantiate and connect widgets ...

        #
        # Parameters Area
        #

        self.segVolumeTotalNode = []

        inputCollapsibleButton = ctk.ctkCollapsibleButton()
        inputCollapsibleButton.text = "Input Volumes"
        self.layout.addWidget(inputCollapsibleButton)

        self.pathText = qt.QLineEdit()
        self.pathText.setToolTip('Specify output path')
        self.layout.addWidget(self.pathText)

        # Layout within the dummy collapsible button
        inputFormLayout = qt.QFormLayout(inputCollapsibleButton)

        loadAbdomenVolumeButton = qt.QPushButton('Load abdomen volume')
        loadAbdomenVolumeButton.connect('clicked()', self.loadVolume)
        inputFormLayout.addRow('Load abdomen volume', loadAbdomenVolumeButton)

        loadSegVolumeButton = qt.QPushButton('Load segmentation volume')
        loadSegVolumeButton.connect('clicked()', self.loadVolume)
        inputFormLayout.addRow('Load segmentation volume', loadSegVolumeButton)

        #
        # input volume selector
        #
        self.inputSelector = slicer.qMRMLNodeComboBox()
        self.inputSelector.nodeTypes = ["vtkMRMLScalarVolumeNode"]
        self.inputSelector.selectNodeUponCreation = True
        self.inputSelector.addEnabled = False
        self.inputSelector.removeEnabled = False
        self.inputSelector.noneEnabled = False
        self.inputSelector.showHidden = False
        self.inputSelector.showChildNodeTypes = False
        self.inputSelector.setMRMLScene(slicer.mrmlScene)
        self.inputSelector.setToolTip("Select the CT abdomen volume.")
        inputFormLayout.addRow("Input Volume: ", self.inputSelector)

        #
        # output volume selector
        #
        self.segmentationSelector = slicer.qMRMLNodeComboBox()
        self.segmentationSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode"]
        self.segmentationSelector.selectNodeUponCreation = True
        self.segmentationSelector.addEnabled = True
        self.segmentationSelector.removeEnabled = True
        self.segmentationSelector.noneEnabled = True
        self.segmentationSelector.showHidden = False
        self.segmentationSelector.showChildNodeTypes = False
        self.segmentationSelector.setMRMLScene(slicer.mrmlScene)
        self.segmentationSelector.setToolTip("Select the ground truth segmentation "
                                             "(if any exists) to compare with prediction.")
        inputFormLayout.addRow("Segmentation Volume: ", self.segmentationSelector)


        font = qt.QFont()
        font.setBold(True)

        # Segment psoas button

        self.segmentationButton = qt.QPushButton('Segment Psoas')
        self.segmentationButton.setFont(font)
        self.segmentationButton.toolTip = 'Segment the selected vertebrae'
        self.segmentationButton.enabled = False

        # Segmentation button connections
        self.segmentationButton.connect('clicked(bool)', self.onSegmentButton)
        self.inputSelector.connect("currentNodeChanged(vtkMRMLNode*)", self.onSelect)


        self.layout.addWidget(self.segmentationButton)

        # Reset segmentations and fiducial markers
        self.resetSegButton = qt.QPushButton('Reset Segmentation')
        self.resetSegButton.toolTip = "Reset fiducial markers"
        self.resetSegButton.setFont(font)
        self.resetSegButton.connect('clicked(bool)', self.onResetSegButton)
        self.layout.addWidget(self.resetSegButton)

        # Save segmentation button
        self.saveButton = qt.QPushButton('Save')
        self.saveButton.toolTip = "Save segmentation"
        self.saveButton.setFont(font)
        self.saveButton.connect('clicked(bool)', self.onSaveButton)

        # Add vertical spacer
        self.layout.addStretch(1)

        # Refresh Apply button state
        self.onSelect()

    def displayLabel(self, seg):
        slicer.util.setSliceViewerLayers(label=seg, labelOpacity=1)

    def cleanup(self):
        pass

    def onSelect(self):
        self.segmentationButton.enabled = self.inputSelector.currentNode() #and self.markerTableSelector.currentNode()

    def onResetSegButton(self):

        for segNode in self.segVolumeTotalNode:
            slicer.mrmlScene.RemoveNode(segNode)

        self.segVolumeTotalNode = []

        individualSeg = slicer.mrmlScene.GetNodesByName('segPrediction')
        num_of_seg = individualSeg.GetNumberOfItems()

        for idx in range(num_of_seg):
            slicer.mrmlScene.RemoveNode(individualSeg.GetItemAsObject(idx))


    def onSegmentButton(self):
        logic = PsoasSegmentationLogic()
        self.segVolumeTotalNode = logic.run(self.inputSelector.currentNode(), self.segVolumeTotalNode)

    def loadVolume(self):
        slicer.util.openAddVolumeDialog()


    def onSaveButton(self):

        seg_filename = self.segCombineVol.GetName()

        save_dir = self.pathText.text

        if save_dir == '':
            print('No path has been entered')
            return

        if os.path.exists(save_dir) is False:
            print('Path entered does not exist. Please enter a proper path.')
            return

        slicer.util.saveNode(self.segCombineVol, save_dir + '/' + seg_filename + '.nii.gz')







class PsoasSegmentationLogic(ScriptedLoadableModuleLogic):
    """This class should implement all the actual
    computation done by your module.  The interface
    should be such that other python code can import
    this class and make use of the functionality without
    requiring an instance of the Widget.
    Uses ScriptedLoadableModuleLogic base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def onClick(self):
        pass

    def dsc(self, y_pred, y_true):

        y_true = y_true.flatten()
        y_pred = y_pred.flatten()

        intersection = np.sum(y_true * y_pred)
        smooth = 1e-7  # So you dont have divide by zero errors
        dsc = (2. * intersection) / (np.sum(y_true) + np.sum(y_pred) + smooth)

        return dsc.mean()

    def resample (self, inputVolume, orig_spacing, output_spacing, inputVolume_origin):
        resample = sitk.ResampleImageFilter()
        resample.SetReferenceImage(inputVolume)
        resample.SetInterpolator(sitk.sitkGaussian)
        #resample.SetInterpolator(sitk.sitkLinear) sitkNearestNeighbor sitkBSpline sitkLabelGaussian sitkGaussian sitkLanczosWindowedSinc

        shapeIn = inputVolume.GetSize()
        spacingIn = inputVolume.GetSpacing()
        newSize = [int(shapeIn[0] * spacingIn[0] / output_spacing[0]),
                   int(shapeIn[1] * spacingIn[1] / output_spacing[1]),
                   int(shapeIn[2] * spacingIn[2] / output_spacing[2])]
        resample.SetSize(newSize)

        resample.SetOutputSpacing(output_spacing)
        resampled_image = resample.Execute(inputVolume)

        return resampled_image

    def padding(self, scan, dim):
        print("padding")
        # pads the image to desired dims
        shape = np.shape(scan)
        print(shape)
        ratio = np.ones(3)
        if shape[0] > dim[0]:
            ratio[0] = dim[0] / shape[0]
        if shape[1] > dim[1]:
            ratio[1] = dim[1] / shape[1]
        if shape[2] > dim[2]:
            ratio[2] = dim[2] / shape[2]
        print(ratio)
        scan = scipy.ndimage.zoom(scan, ratio)
        shape = np.shape(scan)
        print(np.shape(scan))
        padded_array = np.full(shape=dim, fill_value=scan.min())
        x_offset = int((dim[0] - shape[0]) / 2)
        y_offset = int((dim[1] - shape[1]) / 2)
        z_offset = int((dim[2] - shape[2]) / 2)
        padded_array[x_offset:scan.shape[0] + x_offset, y_offset:scan.shape[1] + y_offset, z_offset:scan.shape[2] + z_offset] = scan

        return padded_array, x_offset, y_offset, z_offset, ratio

    def get_output_filenames(self, args):
        in_files = args.input
        out_files = []

        if not args.output:
            for f in in_files:
                pathsplit = os.path.splitext(f)
                out_files.append("{}_OUT{}".format(pathsplit[0], pathsplit[1]))
        else:
            out_files = args.output

        return out_files

    def run(self, inputVolume, segVolumeTotalNode):
        """
        Run the actual algorithm
        """
        inputVolume_origin = inputVolume.GetOrigin()
        inputVolume_spacing = inputVolume.GetSpacing()
        #inputVolume_size = inputVolume.GetImageData().GetDimensions()
        #inputVolume_direction = inputVolume.GetDirection()

        psoas_img = sitkUtils.PullVolumeFromSlicer(inputVolume)
        psoas_img = sitk.Cast(psoas_img, sitk.sitkFloat32)
        #print(psoas_img)
        dim = [64, 128, 128]
        output_spacing = (1.15, 1.15, 2.5)

        #device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        device = torch.device('cpu')
        #device = torch.device('cuda:0')
        model = UNetPsoas()
        #print(os.getcwd())
        model.load_state_dict(torch.load('/home/fullerkj/Repos/myRepos/3d-psoas-segmentor/unet_300_weights.pt'))
        model.to(device=device)
        scan_resampled = self.resample(psoas_img, inputVolume_spacing, output_spacing, inputVolume_origin)
        scan_arr = sitk.GetArrayFromImage(scan_resampled)
        scan = (scan_arr - scan_arr.min()) / (scan_arr.max() - scan_arr.min())
        scan_padded, x_offset, y_offset, z_offset, ratio = self.padding(scan, dim)

        scan_tensor = np.expand_dims(scan_padded, 0)
        scan_tensor = np.expand_dims(scan_tensor, 0)

        scan_tensor = torch.from_numpy(scan_tensor.astype(np.float32)).to(device)
        seg = model(scan_tensor)
        seg = seg[0, 0]

        seg_arr = seg.detach().cpu().numpy()

        seg_arr = scipy.ndimage.zoom(seg_arr, 1/ratio) # to put the original size back

        #test_before = sitk.GetImageFromArray(unpadded_scan.astype(np.float))
        #test_before.CopyInformation(scan_resampled)
        #test_afterresample = sitk.GetImageFromArray(unpadded_scan.astype(np.float))
        #test_afterresample.CopyInformation(scan_resampled)
        #test_afterpad = sitk.GetImageFromArray(unpadded_scan.astype(np.float))
        #test_afterpad.CopyInformation(scan_resampled)
        out_dir = "/home/fullerkj/Repos/myRepos/3d-psoas-segmentor/"
        seg_pred = out_dir + '/' + '-pred_final.nrrd'
        scan_input = out_dir + '/' + '-beforeresample.nrrd'
        scan_afterresample = out_dir + '/' + '-afterresample.nrrd'
        scanpadded = out_dir + '/' + '-afterpad.nrrd'
        sitk.WriteImage(sitk.GetImageFromArray(seg_arr), seg_pred)
        sitk.WriteImage(psoas_img, scan_input)
        sitk.WriteImage(scan_resampled, scan_afterresample)
        sitk.WriteImage(sitk.GetImageFromArray(scan_padded), scanpadded)


        seg_arr = np.where(seg_arr > 0.5, 1, 0)
        print(seg_arr)
        print("seg arr")
        original_shape = np.shape(scan)
        unpadded_scan = seg_arr[x_offset:original_shape[0] + x_offset, y_offset:original_shape[1] + y_offset,
                        z_offset:original_shape[2] + z_offset]
        y_pred_sitk = sitk.GetImageFromArray(unpadded_scan.astype(np.float))
        y_pred_sitk.CopyInformation(scan_resampled)

        resample_back = sitk.ResampleImageFilter()
        resample_back.SetReferenceImage(psoas_img)

        affine = sitk.AffineTransform(3)

        resample_back.SetTransform(affine)

        resample_back.SetInterpolator(sitk.sitkNearestNeighbor)
        y_pred_sitk_full_size = resample_back.Execute(y_pred_sitk)

        segVolumeNode = sitkUtils.PushVolumeToSlicer(y_pred_sitk_full_size,
                                                     name='segPrediction',
                                                     className='vtkMRMLLabelMapVolumeNode')

        segVolumeTotalNode.append(segVolumeNode)

        slicer.util.setSliceViewerLayers(label=segVolumeNode, labelOpacity=1)
        return segVolumeTotalNode





class PsoasSegmentorTest(ScriptedLoadableModuleTest):
    """
  Base class for module tester class.
  Setting messageDelay to something small, like 50ms allows
  faster development time.
  """

    def __init__(self, *args, **kwargs):
        super(PsoasSegmentorTest, self).__init__(*args, **kwargs)

    def delayDisplay(self, message, requestedDelay=None, msec=None):
        """
    Display messages to the user/tester during testing.
    By default, the delay is 50ms.
    The function accepts the keyword arguments ``requestedDelay`` or ``msec``. If both
    are specified, the value associated with ``msec`` is used.
    This method can be temporarily overridden to allow tests running
    with longer or shorter message display time.
    Displaying a dialog and waiting does two things:
    1) it lets the event loop catch up to the state of the test so
    that rendering and widget updates have all taken place before
    the test continues and
    2) it shows the user/developer/tester the state of the test
    so that we'll know when it breaks.
    Note:
    Information that might be useful (but not important enough to show
    to the user) can be logged using logging.info() function
    (printed to console and application log) or logging.debug()
    function (printed to application log only).
    Error messages should be logged by logging.error() function
    and displayed to user by slicer.util.errorDisplay function.
    """
        if hasattr(self, "messageDelay"):
            msec = self.messageDelay
        if msec is None:
            msec = requestedDelay
        if msec is None:
            msec = 100

        slicer.util.delayDisplay(message, msec)

    def runTest(self):
        """
    Run a default selection of tests here.
    """
        logging.warning('No test is defined in ' + self.__class__.__name__)


def DefaultSegSelect():
    SegSelector = slicer.qMRMLNodeComboBox()
    SegSelector.nodeTypes = ["vtkMRMLLabelMapVolumeNode"]
    SegSelector.selectNodeUponCreation = False
    SegSelector.addEnabled = False
    SegSelector.removeEnabled = False
    SegSelector.noneEnabled = True
    SegSelector.showHidden = False
    SegSelector.showChildNodeTypes = False
    SegSelector.setMRMLScene(slicer.mrmlScene)

    return SegSelector
