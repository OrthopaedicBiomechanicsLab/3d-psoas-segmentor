# README #

### What is this repository for? ###

* Module originally built for 3D Psoas Muscle Segmentation on Abdominal CT scans in Prostate Cancer Patients to track 
* progression of sarcopenia.
* Version 1.0

## Dependencies
This project has the following required program dependencies:

**Requires pytorch - tested on torch 1.10.2** - to download go to https://pytorch.org/

*Module was originally built using Slicer 4.11.0-2019-05-01 Nightly release.*

*Tested using Python 3.6.*

*Module was built and tested in Linux - Ubuntu 18.04.5 LTS.*

## How do I get set up?
1) To set this project up, first clone the project into your desired repository from bitbucket.

2) Change the output directory (out_dir) on line 332 and the path to the weights file on line 308.

3) Open Slicer.

4) Navigate to Edit > Application Settings > Modules.

5) Add this module to the path by clicking the ">>" arrows next to "Additional module paths" and clicking "Add".

6) Navigate to the cloned project on your local computer.  Click "Choose" and then click "OK" and reload Slicer.

## Workflow
1) Load in a psoas volume and crop it to the L2/L3 to L4/L5 region using the "Crop Volume" module.

2) Find and open the module under the search tab.

3) Select your cropped volume in the *Input Volume* selector node

4) Click the "Apply" button to segment the psoas.

## Future Work
Ideally it would be great to not have paths that need to be changed within the code for the PsoasSegmentor module. This 
isn't good practice.

### Who do I talk to? ###

* Sunnybrook Research Institute - Orthopaedics Biometrics Labratory - Cari Whyne or Michael Hardisty
